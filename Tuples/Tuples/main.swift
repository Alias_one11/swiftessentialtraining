import Foundation

func main() -> Void
{
    divT("Tuples In Swift IOS")
    print("-----------------------------------------------------------")
    
    /// - ©Example #1 Simple tuple
    //*©-----------------------------------------------------------©*/
    let uppercutAttack: (String, Int, Bool) = ("Uppercut Smash", 25, true)
    print("©Tuple uppercutAttack: (String, Int, Bool): \(uppercutAttack)\n")
    
    print("1a. Accessing uppercutAttack by index[0]: \(uppercutAttack.0)")
    print("1b. Accessing uppercutAttack by index[1]: \(uppercutAttack.1)")
    print("1c. Accessing uppercutAttack by index[2]: \(uppercutAttack.2)")

    // - ©Unpacking our tuple into its own named variable
    let (attack, damage, rechargeable) = uppercutAttack
    print("1d. (attack) = uppercutAttack.0: \(attack)")
    print("1e. (damage) = uppercutAttack.1: \(damage)")
    print("1f. (rechargeable) = uppercutAttack.2: \(rechargeable)")

    /// - ©Example #2 Naming tuple values
    //*©-----------------------------------------------------------©*/
    var shieldStomp: (name: String, damage: Int, rechargeable: Bool)
    shieldStomp.name = "Alias The Great"
    shieldStomp.damage = 80
    shieldStomp.rechargeable = true
    
    print("2a. shieldStomp.name: \(shieldStomp.name)")
    print("2b. shieldStomp.damage: \(shieldStomp.damage)")
    print("2c. shieldStomp.rechargeable: \(shieldStomp.rechargeable)")

    
    
    /// - ©Example #3 literal tuple declaration
    //*©-----------------------------------------------------------©*/
    let planetSmash = (name: "Planet Smash", damage: 45, rechargeable: true)
    print("3a. planetSmash.name: \(planetSmash.name)")
    print("3b. planetSmash.damage: \(planetSmash.damage)")
    print("3c. planetSmash.rechargeable: \(planetSmash.rechargeable)")
    
    print("-----------------------------------------------------------")
    print()
}
// RUNS-APP
main()
