import Foundation

func main() -> Void
{
    divT("Core Arrays")
    print("-----------------------------------------------------------")
    
    /// - ©Example #1 Changing & appending items
    //*©-----------------------------------------------------------©*/
    var characterClass = ["Ranger", "Paladin", "Druid"]
    // - ©Printing what we initially have in the character class
    arrayToString(arrayName: "Initial-->characterClass", array: characterClass)
    print()
    
    characterClass.append("Gunslinger")
    
    // - ©Checking if our Gunslinger was appended to our array
    print("1a. characterClass.append index[3]: \(characterClass[3])")
    
    // - ©Appending two more values with the += operators
    characterClass += ["Healer", "Berserker"]
    
    print("""
        2b. characterClass += index[4]: \(characterClass[4])
        2c. characterClass += index[5]: \(characterClass[5])
        """)
    
    /// - ©Example #2 Inserting & removing items at a certain index
    //*©-----------------------------------------------------------©*/
    characterClass.insert("Beast Master", at: 2)
    print("2a. characterClass.insert[2]: \(characterClass[2])")
    
    // - ©Removing an item
    characterClass.remove(at: 1)// Removed Paladin
    print("2b. characterClass.remove(at: 1): Removed Paladin")
    
    /// - ©Example #3 Arraging the array order & Querying values
    //*©-----------------------------------------------------------©*/
    let reversedClass = characterClass.reversed() as [String]
    arrayToString(arrayName:"3a. characterClass.reverse()",
                  array: reversedClass)
    
    // - ©Sorting in alphabetical order
    let sortedClass = characterClass.sorted()
    arrayToString(arrayName: "3b. characterClass.sorted()",
                  array: sortedClass)
    
    // - ©.contains: Will return a `Bool` of true/false
    let containsClass = characterClass.contains("Beast Master")
    print("3c. characterClass.contains(Beast Master): \(containsClass)")

    /// - ©Example #4 2d arrays & subscripts
    //*©-----------------------------------------------------------©*/
    // - ©2d or jagged arrays
    let skillTree: [[String]] = [
        ["Attack+", "Barrage", "Heavy Hitter"],
        ["Guard+", "Evasion", "Run"]
    ]
    
    print("4a. skilltree: \(skillTree.debugDescription)")
    
    // - ©Accessing the array in a array
    arrayToString(arrayName: "4b. var skillTree: [[String]] index[0]",
                  array: skillTree[0])
    arrayToString(arrayName: "4c. var skillTree: [[String]] index[1]",
                  array: skillTree[1])
    
    // - ©Accessing the array in an array & then its items
    extract2D(strMsg: "4d. skillTree[0][1]",
              index: 0, itemIndex: 1, skillTree)

    extract2D(strMsg: "4e. skillTree[1][2]",
              index: 1, itemIndex: 2, skillTree)
    
    //*©-----------------------------------------------------------©*/
    // - ©Printing after working with characterClass
    arrayToString(arrayName: "\nPost-->characterClass",
                  array: characterClass)
    
    print("-----------------------------------------------------------")
    print()
}
// RUNS-APP
main()


