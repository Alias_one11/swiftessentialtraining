import Foundation

func main() -> Void
{
    divT("For In Loop")
    print("-----------------------------------------------------------")
    /// - ©Example #1 Strings & arrays
    let playerGreeting = "Hola Mi Gente!"
    let armorType = ["Heavy Plate", "Hunter Gear", "Mage Robes"]
    let weapons = ["Longwood": 150, "Dagger": 25, "Mace": 75]
    
    print("1. strChar:", terminator: " ")

    for strChar in playerGreeting {
        //__________
        print("\(strChar)", terminator: "-")
    }
    
    print("\n2. armor:", terminator: " ")
    
    for armor in armorType {
        //__________
        print(armor, terminator: " | ")
    }
    
    /// - ©Example #2 Dictionary key/value pairs
    //*©-----------------------------------------------------------©*/
    print("\n3. weaponKey:", terminator: " ")
    // - ©Keys
    for weaponKey in weapons.keys {
        //__________
        print(weaponKey, terminator: " | ")
    }
    // - ©Values
    print("\n4. weaponValue:", terminator: " ")
    
    for weaponValue in weapons.values {
        //__________
        print(weaponValue, terminator: " | ")
    }
    // - ©Keys:Values
    print("\n5. weapon: damage-->", terminator: " ")
    
    for (weapon, damage) in weapons {
        //__________
        print("\(weapon): \(damage)", terminator: " | ")
    }
    
    /// - ©Example #3 Using range
    //*©-----------------------------------------------------------©*/
    print("\n6. Closed range:", terminator: " ")
    // - ©Closed range: Includes first & last number plus everything in between
    for indexNumber in 1...10 {
        //__________
        print(indexNumber, terminator: " | ")
    }
    
    print("\n7. One sided range:", terminator: " ")
    // - ©armorType from index 0...until the last index
    for armor in armorType[0...] {
        //__________
        print(armor, terminator: " | ")
    }
    
    print("\n8. Half open range:", terminator: " ")
    // - ©From 1..all the way to 9 but less then<10
    for indexNumber in 1..<10 {
        //__________
        print(indexNumber, terminator: " | ")
    }
    
    print("\n9. Half open range with a dictionary foo[..<foo.count]:\n", terminator: "   | ")
    // - ©Will not go past the array index when using ..<armorType.count
    for armor in armorType[..<armorType.count] {
        //__________
        print(armor, terminator: " | ")
    }
    
    /// - ©Example #4 guard statement
    //*©-----------------------------------------------------------©*/
    let shopItems: [String : Int] = ["Magic Wand": 10, "Iron Helm": 5, "Excalibur": 1000]
    let currentGold = 16
    
    print("\n\n$. [BONUS]-->Using the gaurd statement")
    
    for (item, price) in shopItems {
        //__________
        guard currentGold >= price else {
            //__________
            print("1. You can't afford item: \(item)")
            continue
        }
    }
    
    var testNum = 7
    test(num: &testNum)
    
    print("\n-----------------------------------------------------------\n")
}
// RUNS-APP
main()

func test(num: inout Int) {
    num *= 2
    print("2. Swift by &reference inout: \(num)")
}
